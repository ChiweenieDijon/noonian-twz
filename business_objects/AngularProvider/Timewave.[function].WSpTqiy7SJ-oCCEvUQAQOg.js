function(NoonWebService, DateUtil, $q) {
    
    const minWindow = 0.00003;
    
    const dpy = DateUtil.daysPerYear; //days per year, per gregorian calendar
    
    const eraTransition = DateUtil.eraTransition;
    
    const Timewave = function(zeroDate, numberSet, waveFactor, resolution, invertPostZeroDate) {
        
        this.setParameters(zeroDate, numberSet, waveFactor, resolution, invertPostZeroDate);
        
        this.undoStack = [];
    };
    
    Timewave.NumberSets = ['kelley', 'watkins', 'sheliak', 'huangti'];
    Timewave.NumberSetLabels = {
        kelley:'Kelley',
        watkins:'Watkins',
        sheliak:'Sheliak',
        huangti:'Huang Ti'
    };
    
    
    Timewave.prototype.setParameters = function(zeroDate, numberSet, waveFactor, resolution, invertPostZeroDate) {
        zeroDate = this.zeroDate = moment(zeroDate);
        this.numberSet = numberSet || sheliak;
        this.waveFactor = waveFactor || 64;
        
        this.resolution = resolution || 500;  //# of points to generate within the window
        
        this.invertPostZeroDate = invertPostZeroDate;
        
        
        this.daysToEraTransition = zeroDate.diff(eraTransition, 'days');
        this.yearsToEraTransition = this.daysToEraTransition/dpy;
        
        if(this.windowRange) {
            return this.setWindow(this.windowRange);
        }
        else {
            return $q.when(null);
        }
    };
    
    
    
    /**
     * Return {era, year, month, day} object;
     * (era=BC -> omit month/day)
     */
    Timewave.prototype.getDateForDtz = function(dtz) {
        
        if(dtz <= this.daysToEraTransition) {
            //Still in AD era
            var d = moment(this.zeroDate).subtract(dtz, 'days');
            return {
                era:'AD',
                year:d.get('year'),
                month:d.get('month'),
                day:d.get('date')
            };
        }
        else {
            var daysBc = dtz - this.daysToEraTransition;
            return {
                era:'BC',
                year:(daysBc/dpy)
            };
        }
        
    };
    
    Timewave.prototype.getDtzForDate = function(date) {
        return moment(this.zeroDate).diff(moment(date), 'days');
    };
    
    
    Timewave.prototype.getWindowSize = function() {
        var wr = this.windowRange;
        if(wr) {
            return Math.abs(wr.start - wr.end);
        }
        return -1;
    };
    
    /*
        A) full standard date going back to 1/1/0001
        B) "BC" year w/ decimal going back 99999 years
        C) "Years Before Epoch" count w/ suffix
     */
    Timewave.prototype.formatDtz = function(dtz, abbrev) {
        var numYears = (dtz / DateUtil.unitScale.years);
        var numYearsBC = numYears - this.yearsToEraTransition;
        
        if(numYearsBC > 99999) {
            //Display as "years before epcoch"
            return DateUtil.abbreviateLargeNum(numYears)+' yrs'
        }
        else if(numYearsBC > 0) {
            return DateUtil.abbreviateLargeNum(numYearsBC)+' BCE';
        }
        else {
            return moment(this.getDateForDtz(dtz)).format('M/D/YYYY');
        }
    };
    
    
    /**
     * range: {start, end} - values as days-to-zerodate
     */
    Timewave.prototype.setWindow = function(range, isUndo) {
        // console.log('setWindow', range);
            
        var saved = this.windowRange;
        
        this.windowRange = range;
        
        if(!isUndo && saved && saved.start && saved.end && saved.start !== range.start && saved.end !== range.end) {
            this.undoStack.push(saved);
        }
        
        
        return NoonWebService.call('public/computeWindow', {
            numberSet:this.numberSet,
            startWindow:range.start,
            endWindow:range.end,
            waveFactor: this.waveFactor,
            numPoints:this.resolution,
            invert:this.invertPostZeroDate
        }).then((data) => {
            //returns points in parallel arrays: dtz and vals
            //we'll convert to {x,y} objects
            
            let max = {y:-1};
            let min = {y:Number.MAX_SAFE_INTEGER};
            
            let dataArr = this.dataPoints = [];
            for(let i=0; i < data.dtz.length; i++) {
                
                let p = { x:data.dtz[i], y:data.vals[i] };
                dataArr.push(p);
                
                if(p.y < min.y) {
                    min = p;
                }
                if(p.y > max.y) {
                    max = p;
                }
            }
            
            this.windowStats = {
                min, 
                max,
                minDate:this.formatDtz(min.x),
                maxDate:this.formatDtz(max.x)
            };
            
            return dataArr;
        });
        
    };//end setWindow
    
    
    Timewave.prototype.undo = function() {
      if(this.undoStack.length) {
          return this.setWindow(this.undoStack.pop(), true);
      } 
    };
    
    
    //shrink the number of days displayed in the window by the given percentage
    Timewave.prototype.zoomIn = function(percent, centerDtz) {
        var percentChg = percent/100;
        var daysDisplayed = this.windowRange.start - this.windowRange.end;
        
        if(daysDisplayed < minWindow) {
            alert('Maximum zoom reached');
            return $q.when(false);
        }
        
        
        var newRange = (1-percentChg)*daysDisplayed;
        var center = centerDtz || this.windowRange.end+(daysDisplayed/2);
        var fromCenter = newRange/2;
        
        var r = {
            start:(center + fromCenter),
            end:(center - fromCenter)
        };
        
        return this.setWindow(r);
    };
    
    //increase the number of days displayed in the window by the given percentage
    Timewave.prototype.zoomOut = function(percent) {
        var percentChg = percent/100;
        var daysDisplayed = this.windowRange.start - this.windowRange.end;
        
        var newRange = (1+percentChg)*daysDisplayed;
        var center = this.windowRange.end+(daysDisplayed/2);
        var fromCenter = newRange/2;
        
        var r = {
            start:(center + fromCenter),
            end:(center - fromCenter)
        };
        
        if(r.end < 0 && !this.invertPostZeroDate) {
            //shift it over
            r.start -= r.end;
            r.end = 0;
        }
        
        return this.setWindow(r);
    };
    
    Timewave.prototype.shiftLeft = function(percent) {
        var percentChg = percent/100;
        var daysDisplayed = this.windowRange.start - this.windowRange.end;
        var amount = daysDisplayed * percentChg;
        
        var r = {
            start:this.windowRange.start+amount,
            end:this.windowRange.end+amount
        };
        
        return this.setWindow(r);
    };
    
    Timewave.prototype.shiftRight = function(percent) {
        var percentChg = percent/100;
        var daysDisplayed = this.windowRange.start - this.windowRange.end;
        var amount = daysDisplayed * percentChg;
        
        
        var r = {
            start:this.windowRange.start-amount,
            end:this.windowRange.end-amount
        };
        
        return this.setWindow(r);
    };
    
    Timewave.prototype.upResonance = function() {
        var factor = this.waveFactor;
        var r = {
            start:factor*this.windowRange.start,
            end:factor*this.windowRange.end
        };
        
        return this.setWindow(r);
    };
    
    Timewave.prototype.downResonance = function() {
        var factor = this.waveFactor;
        var r = {
            start:this.windowRange.start/factor,
            end:this.windowRange.end/factor
        };
        
        return this.setWindow(r);
    };
    
    
    
    Timewave.prototype.getFractalDimension = function() {
        return NoonWebService.call('public/computeFractalDimension', {
            numberSet:this.numberSet,
            startWindow:this.windowRange.start,
            endWindow:this.windowRange.end,
            waveFactor: this.waveFactor,
            numPoints:this.resolution,
            invert:this.invertPostZeroDate
        })
        // .then(data=>{
        //     console.log(data);
        //     return data;
        // });
    }
    
    return Timewave;
}