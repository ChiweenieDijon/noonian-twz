function($uibModal, $rootScope, NoonWebService) {
    
    const EventEditorDialog = {};
    
    const monthNames = moment.monthsShort();
    const months = [];
    
    for(var i=0; i < 12; i++) {
        months.push({
            number:i,
            label:monthNames[i]
        });
    }
    
    const multipliers = [{
        label:'(no multiplier)',
        number:1,
        suffix:'.0'
    },
    {
        label:'thousand',
        number:1000,
        suffix:',000'
    },
    {
        label:'million',
        number:1000000,
        suffix:',000,000'
    },
    {
        label:'billion',
        number:1000000000,
        suffix:',000,000,000'
    }
    ];
    
    const processDate = function(dateObj) {
        
        var mult = dateObj && dateObj.multiplier && dateObj.multiplier.number;
        
        if(dateObj && dateObj.year && mult > 1) {
            dateObj.year = mult*dateObj.year;
        }
        delete dateObj.multiplier;
    };
    
    
    const showEditorDialog = function(marker, title) {
        var modalInstance;
        var scope = $rootScope.$new(true);
        
        scope.title = title;
        scope.obj = marker;
        
        scope.months = months;
        scope.multipliers = multipliers;
        
        modalInstance = $uibModal.open({
            templateUrl:'twz/event_editor_dialog.html',
            size:'lg',
            scope: scope
        });
        
        
        scope.getTags = function(val) {
            if(val) {
                return NoonWebService.call('twz/getTaglistTypeahead', {val});
            }
        };
        
        scope.submit = function() {
            var toSubmit = angular.copy(scope.obj);
            
            processDate(toSubmit.startDate);
            if(toSubmit.endDate.year) {
                processDate(toSubmit.endDate);
            }
            else {
                delete toSubmit.endDate;
            }
            
            
            NoonWebService.post('twz/saveTimeMarker', {}, toSubmit).then(function(result) {
                console.log(result);
                modalInstance.close();
            });
        };
        
        scope.delete = function() {
            if(!scope.obj._id) {
                return;
            }
            var title = scope.obj.name || 'untitled';
            if(confirm('Are you sure you want to delete "'+title+'"?')) {
                NoonWebService.call('twz/deleteTimeMarker', {id:scope.obj._id}).then(function(result) {
                    console.log(result);
                    modalInstance.close();
                });
            }
        };
        
        
        return modalInstance.result.then(()=>{
            console.log(scope.obj);
            
        },
        err=>{
            console.log('dismiss!');
        });
    };
    
    
    EventEditorDialog.new = function(startDate) {
        if(startDate) {
            console.log(startDate);
            startDate.multiplier = multipliers[0];
        }
        else {
            startDate = {era:'AD', multiplier:multipliers[0]};
        }
        
        const marker = {
            startDate,
            endDate:{era:'AD', multiplier:multipliers[0]}
        };
        
        
        return showEditorDialog(marker, 'Create New Event Marker')
    };
    
    
    EventEditorDialog.edit = function(marker, timewave) {
        console.log(marker);
        if(marker.days_start) {
            marker.startDate = timewave.getDateForDtz(marker.days_start);
            marker.startDate.multiplier = multipliers[0];
        }
        else {
            marker.startDate = {era:'AD', multiplier:multipliers[0]}
        }
        if(marker.endDate) {
            marker.endDate = timewave.getDateForDtz(marker.days_start);
            marker.endDate.multiplier = multipliers[0];
        }
        else {
            marker.endDate = {era:'AD', multiplier:multipliers[0]}
        }
        
        return showEditorDialog(marker, 'Edit Event Marker');
    };
    
    return EventEditorDialog;
}