function () {
    
    const monthNames = moment.monthsShort();
    const months = [];
    
    for(var i=0; i < 12; i++) {
        months.push({
            number:i,
            label:monthNames[i]
        });
    }
    
    const multipliers = [
        {
            label:'(no multiplier)',
            number:1,
            suffix:'.0'
        },
        {
            label:'thousand',
            number:1000,
            suffix:',000'
        },
        {
            label:'million',
            number:1000000,
            suffix:',000,000'
        },
        {
            label:'billion',
            number:1000000000,
            suffix:',000,000,000'
        }
    ];
    
    return {
        templateUrl:'twz/directive/date_editor.html',
        
        restrict: 'E',
        
        scope: {
            date: '='
        },
        
        
      
      controller: function($scope) {
          
          if(!$scope.date.era) {
              $scope.date.era = 'AD'
          }
          
          $scope.multipliers = multipliers;
          $scope.months = months;
          
          $scope.setAD = function() {
              $scope.date.era = 'AD';
              $scope.date.multiplier = multipliers[0];
          };
          
          $scope.setBC = function() {
              $scope.date.era = 'BC';
          }
          
      }
      
    };
  }