function ($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.onScroll);
        element.bind('wheel', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
}