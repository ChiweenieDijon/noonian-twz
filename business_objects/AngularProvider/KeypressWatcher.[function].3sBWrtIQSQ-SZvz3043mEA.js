function ($document, $timeout) {
    
    var keyMap = {};
    var disabled = false;
    
    $document.on('keydown', function(event) {
        // console.log('keydown', event.which);
        if(disabled) return;
        
        var toCall = keyMap[event.which];
        if(toCall && toCall.length) {
            toCall.forEach(function(f) {
                $timeout(f.bind(null, event));
            });
            event.stopPropagation();
            event.preventDefault();
        }
    });
    
    var watch = function(keyCode, fn) {
        keyMap[keyCode] = keyMap[keyCode] || [];
        keyMap[keyCode].push(fn);
    };
    
    var unWatch = function(fn) {
        Object.keys(keyMap).forEach(function(kc) {
            var fnArr = keyMap[kc];
            for(var i=0; i < fnArr.length; i++) {
                if(fnArr[i] === fn) {
                    fnArr.splice(i, 1);
                    break;
                }
            }
        });
    };
    
    return {
        disable: function() {disabled = true;},
        enable: function() {disabled = false;},
        
        onEnter:function(fn)    { watch(13, fn); },
        onSpacebar:function(fn) { watch(32, fn); },
        onEsc:function(fn)      { watch(27, fn); },
        onLeft:function(fn)     { watch(37, fn); },
        onRight: function(fn)   { watch(39, fn); },
        onUp:function(fn)     { watch(38, fn); },
        onDown: function(fn)   { watch(40, fn); },
        onKeycode: watch,
        off: unWatch
    };
}