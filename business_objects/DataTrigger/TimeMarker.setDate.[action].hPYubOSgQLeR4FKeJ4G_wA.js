function (DateUtil) {
    if(this.date_start && !this.days_start) {
        this.days_start = DateUtil.dateToEpochDays(this.date_start);
    }
    if(this.date_end && !this.days_end) {
        this.days_end = DateUtil.dateToEpochDays(this.date_end);
    }
}