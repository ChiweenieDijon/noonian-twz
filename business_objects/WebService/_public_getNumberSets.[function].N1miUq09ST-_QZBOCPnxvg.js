function(db, req, _) {
    const queryObj = {
        type:'quantification_values',
        $or:[
            {owner:null},
            {'owner._id':req.user._id}
        ]
    };
    
    return db.NumberSet.find(queryObj, {name:1,key:1}).lean().then(nsList=>{
        _.forEach(nsList, ns=>{
            delete ns._id;
        });
        return nsList;
    });
}