function(db, queryParams, req) {
    const id = queryParams.id;
    
    if(!id) {
        throw 'missing required param(s)';
    }
    
    return db.TimeMarker.findOne({_id:id}).then(tm=>{
        if(!tm.created_by || tm.created_by._id !== req.user._id) {
            throw 'Permissions failure';
        }
        
        return tm.remove().then(()=>{
            return {result:'success'}
        });
    })
}