function(db, postBody, Q, _, DateUtil, req) {
    const input = postBody;
    
    if(!input.startDate || !input.startDate.year || !input.name) {
        throw 'missing required param(s)';
    }
    
    
    if(input._id) {
        return db.TimeMarker.findOne({_id:input._id}).then(tm=>{
            if(!tm.created_by || tm.created_by._id !== req.user._id) {
                throw 'Permissions failure';
            }
            _.assign(tm, input);
            
            tm.days_start = DateUtil.dateToEpochDays(input.startDate);
            tm.days_end = DateUtil.dateToEpochDays(input.endDate);
            
            return tm.save().then(()=>{
                return {result:'success'}
            });
        })
    }
    else {
        
        const tm = new db.TimeMarker({
            created_by:req.user,
            public:input.public,
            name:input.name,
            description:input.description,
            link:input.link,
            days_start:DateUtil.dateToEpochDays(input.startDate),
            days_end:DateUtil.dateToEpochDays(input.endDate)
        });
        
        return tm.save().then(()=>{
            return {result:'success'}
        });
    }
}