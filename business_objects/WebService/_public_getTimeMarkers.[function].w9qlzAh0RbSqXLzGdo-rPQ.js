function (db, req, queryParams, DateUtil, MarkerUtil, _) {
    const moment = require('moment');
    
    
    var startWindow = +queryParams.startWindow;
    var endWindow = +queryParams.endWindow;
    var zd = queryParams.zeroDate;
    var resolution = queryParams.resolution || 500;
    
    
    //compute shift from zd to our epoch
    const startDays = DateUtil.shiftToEpoch(startWindow, zd);
    const endDays = DateUtil.shiftToEpoch(endWindow, zd);
    
    const userId = req.user && req.user._id;
    var userFilter = {'public':true};
    if(userId) {
        userFilter = {$or:[
            userFilter,
            {'created_by._id':userId}
        ]};
    }
    
    var queryObj = {
        $and:[
            {days_start:{$gt:endDays}},
            {days_start:{$lt:startDays}},
            userFilter
        ]
    };
    
    // console.log(JSON.stringify(queryObj, null, 1));
    
    return db.TimeMarker.find(queryObj).sort({days_start:-1}).lean().exec().then(function(markers) {
        var epochDiff = DateUtil.epochDiff(zd);
        
        //Event dates are stored as # of days to unix epoch; shift value to be relative to specified zero date:
        _.forEach(markers, m=>{
            m.days_start = m.days_start + epochDiff;
            if(m.days_end) {
                m.days_end = m.days_end + epochDiff;
            }
            
            if(m.created_by && m.created_by._id === userId) {
                m.isEditable = true;
            }
        });
        
        var grouped = MarkerUtil.createGroupings(markers, startDays, endDays, resolution);
        // console.log(grouped);
        
        return grouped;
    });
    
}