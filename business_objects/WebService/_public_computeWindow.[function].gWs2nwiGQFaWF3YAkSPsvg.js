function (db, TimewaveCalc, queryParams, req) {
    
    var numberSetName = queryParams.numberSet;
    var dtz = +queryParams.startWindow;
    var endDtz = +queryParams.endWindow;
    var wf = +queryParams.waveFactor;
    var invert = (queryParams.invert === 'true');
    
    let minDiff = 0.00002;
    
    
    var numPoints = +queryParams.numPoints;
    
    var numDays = dtz - endDtz;
    
    if(numDays < minDiff) {
        throw 'window too small';
    }
    
    const queryObj = {
        $or:[
            {key:numberSetName, owner:null},
            {key:numberSetName, 'owner._id':req.user._id}
        ]
    };
    
    return db.NumberSet.findOne(queryObj).then(nsObj=>{
        if(!nsObj || !nsObj.values || nsObj.values.length !== 384) {
            throw 'invalid numberset: '+numberSetName;
        }
        
        // numPoints * stepMin = total span in minutes = numDays*24*60
        const stepMin = (numDays*24*60) / numPoints;
        return TimewaveCalc.computeWindow(nsObj.values, dtz, endDtz, stepMin, wf, invert);
    });
    
    // return TimewaveCalc.computeWindow(numberSetName, dtz, endDtz, stepMin, wf);
}