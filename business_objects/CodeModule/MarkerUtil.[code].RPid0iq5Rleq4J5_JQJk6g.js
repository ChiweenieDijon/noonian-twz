function(_) {
    const exports = {};
    
    
    var MarkerGroup = function(initItem) {
        this.markers = [];
        this.isGroup = true;
        
        if(initItem) {
            this.days_start = initItem.days_start;
            this.markers.push(initItem);
            initItem.grouped = true;
        }
    };
    
    MarkerGroup.prototype.toJSON = function() {
        return {
            days_start:this.days_start,
            markers:this.markers,
            isGroup:true
        }
    };
    
    MarkerGroup.prototype.add = function(item) {
        let len = this.markers.push(item);
        item.grouped = true;
        this.days_start = _.sum(_.pluck(this.markers, 'days_start'))/len;
    };
    
    /**
     * group events that are sufficiently near one another within a given window
     */
    exports.createGroupings = function(markers, windowStart, windowEnd, windowResolution, nearnessThresh) {
        
        nearnessThresh = nearnessThresh || 5;
        
        //Create groupings: if events are close enough to one another, add them to a group
        var stepWidth = (windowStart-windowEnd)/windowResolution;
        var distThresh = nearnessThresh*stepWidth;
        
        var result = [];
        //Markers are sorted in descending order
        for(let i=0; i < markers.length; i++) {
            var curr = markers[i];
            if(curr.grouped) {
                continue;
            }
            
            for(let j=i+1; j < markers.length; j++) {
                var neighbor = markers[j];
                var dist = curr.days_start - neighbor.days_start;
                if(dist < distThresh) {
                    if(!curr.isGroup) {
                        curr = new MarkerGroup(curr);
                    }
                    curr.add(neighbor);
                }
                else {
                    break;
                }
            }
            
            result.push(curr);
        }
        
        return result;
    };
    
    
    return exports;
}