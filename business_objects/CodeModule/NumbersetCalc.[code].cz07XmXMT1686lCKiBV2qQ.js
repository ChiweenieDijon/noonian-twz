function () {
    const exports = {};
    
    //First Order of Difference
    // (0th element is the loop-around transition 64->0)
    const FOD = exports.FOD = [  
        3, 
        6,2,4,4,4,3,2,4,2,4,6,2,2,4,2,2,
        6,3,4,3,2,2,2,3,4,2,6,2,6,3,2,3,
        4,4,4,2,4,6,4,3,2,4,2,3,4,3,2,3,
        4,4,4,1,6,2,2,3,4,3,2,1,6,3,6,3
    ];
    
    //Generate the data points for the shifted reverse wave
    exports.generateReverseWavePoints = function() {
        //Rotate 180 degrees around axis perpendicular to the plane 
        // and shift to line up closure points
        //or, equivalently:
        //(1) flip horizontally (reverse the order)
        //(2) flip vertically (negate the sign)
        //(3) shift up to line up closure endpoints vertically
        //(4) shift left to line up closure points horizontally
        
        //Steps 1-3:
        var reversed = [];
        for(let i=FOD.length-1; i > 0; i--) { //omit the 0th "loop-around" copy for now
            reversed.push(-1*FOD[i]+9);
        }
        
        //step 4:
        var result = [];
        for(let i=1; i < reversed.length; i++) {
            result[i-1] = reversed[i];
        }
        // first one gets wrapped around to the end
        result.push(reversed[0]);
        
        //finally, add the "wrap-around" copy for consistency
        result.push(result[0]);
        
        return result;
        
    };
    
    return exports;
    
}