function ($controllerProvider) {
    
    
    $controllerProvider.register('TwzController', function($scope, $http, $location, $window, $q, NoonWebService, $timeout, ResizeWatcher, EventEditorDialog, Timewave, DateUtil, KeypressWatcher, $document, $cookies) {
        
        
        //////////////////////////////////////////////////
        // Set up scope and input defaults
        //////////////////////////////////////////////////
        
        //Timewave params
        //default 12/21/2012 6 a.m. Colombian time (GMT-5)
        var zeroDateStr = '2012-12-21T06:00:00.000-05:00';
        $scope.zeroDateJs = new Date(zeroDateStr);
        
        $scope.numberSet = 'sheliak';
        $scope.waveFactor = 64;
        $scope.resolution = 500; //datapoints on chart
        
        //Date/range picker
        $scope.rangePicker = 'dist';
        
        $scope.displayFromDate = new Date('2002-12-21T06:00:00.000-05:00');
        $scope.displayToDate = new Date(zeroDateStr);
        
        $scope.displayFromDtz = {
            magnitude:10,
            unit:'years'
        };
        $scope.displayToDtz = {
            magnitude:0,
            unit:'days'
        };
        
        $scope.enableMouseCursor = true;
        $scope.zoomPercent = 20;
        $scope.shiftPercent = 10;
        
        $scope.durationUnits = DateUtil.durationUnits;
        
        
        //Auth/login
        $scope.permissions = {
            eventCreate:false
        };
        
        NoonWebService.call('twz/getMyRoles').then(function(roles) {
            if(roles && roles.indexOf('SUBMITTER') > -1) {
                $scope.permissions.eventCreate = true;
            }
        });
        
        NoonWebService.call('public/getNumberSets').then(function(numberSets){
            $scope.numberSets = numberSets;
        });
        
        $scope.logout = function() {
            $cookies.remove('access_token');
            $window.location.reload(true);
        };
        
        //Display/layout
        const dispState = $scope.dispState = {
            leftSidebarCollapsed:false,
            rightSidebarCollapsed:false,
            showEvents:false
        };
        
        
        
        
        
        const setZeroDateDisp = ()=>{
            $scope.zeroDateDisp = moment($scope.zeroDateJs).format('M/D/YYYY');
        };
        setZeroDateDisp();
        
        
        //Marker that the user can drop on the chart by clicking
        var userMarkerIdx = -1;
        var userMarker = {
            type:'line',
            mode:'vertical',
            scaleID:'primary-x-axis',
            borderColor:'green',
            borderWidth:1,
            label:{
                content:'><',
                fontSize:10,
                position:'bottom',
                yAdjust:5,
                enabled:true
            }
        };
        
        
        const timewave = $scope.timewave = new Timewave(zeroDateStr, $scope.numberSet, $scope.waveFactor, $scope.resolution, $scope.invertPostZeroDate);
        
        
        
        // var yearsToEraTransition;
        // $scope.$watch('zeroDateJs', ()=>{
        //     setZeroDateDisp();
        //     yearsToEraTransition = moment($scope.zeroDateJs).diff(moment('0001-01-01'), 'days')/dpy;
        // });
        
        
        
        const dtzToLabel = $scope.dtzToLabel = timewave.formatDtz.bind(timewave); 
        
        
        //convert dtz to date
        const getTickLabel = function(val, idx, values) {
            return timewave.formatDtz(val);//getDateForDtz(val).format('M/D/YY');
        };
        const getTooltipLabel = function(tooltipItem, data) {
            // console.log(tooltipItem, data);
            return timewave.formatDtz(tooltipItem.xLabel)
        }
        
        
        const stats = {
            left:{header:'Left', date:'', dtz:'', value:''},
            marker:{header:'Marker', date:'', dtz:'', value:''},
            cursor:{header:'Mouse', date:'', dtz:'', value:''},
            right:{header:'Right', date:'', dtz:'', value:''}
        };
        
        $scope.statRows = [
            stats.left,
            stats.marker,
            stats.cursor,
            stats.right,
        ];
        
        const windowStats = $scope.windowStats = {
            maxDate:'',
            minDate:''
        };
        
        const setStat = function(row, dataPoint) {
            let r = stats[row];
            if(dataPoint) {
                r.date = dtzToLabel(dataPoint.x)
                r.dtz = dataPoint.x;
                r.value = dataPoint.y;
            }
            else {
                r.date = null;
                r.dtz = 0;
                r.value = 0;
            }
        };
        
        
        
        var myChart;
        const getDataPoint = function(idx) {
            return myChart.data.datasets[0].data[idx];
        };
        
        
        var tooltipEl = document.getElementById('chartjs-tooltip');
        
        //Called when rendering the tooltip
        //  also used to used to track mouse position
        const customTooltip = function (tooltip) {
            const dp = tooltip.dataPoints;
            
            if(dp && dp.length) {
                var dataPoint = getDataPoint(dp[0].index);
                $scope.$apply(function(){
                    $scope.hoverPoint = dataPoint;
                    setStat('cursor', dataPoint);
                });
            }
            
            
            if(!$scope.enableMouseCursor && tooltipEl) {
                tooltipEl.style.opacity = 0;
                return;
            }
            // Create element on first render
            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                // tooltipEl.innerHTML = '';
                this._chart.canvas.parentNode.appendChild(tooltipEl);
            }
            
            var positionY = this._chart.canvas.offsetTop;
			var positionX = this._chart.canvas.offsetLeft;

			// Display, position, and set styles for font
			tooltipEl.style.opacity = 0.9;
			tooltipEl.style.left = positionX + tooltip.caretX + 'px';
			tooltipEl.style.top = positionY + tooltip.caretY + 'px';
			
        };
        
        const onChartClick = function(evt) {
            // console.log('chartClick', evt);
            if($scope.hoverPoint) {
                $scope.chartClicked($scope.hoverPoint, evt.shiftKey);
            }
            return true;
        };
        
        $scope.mouseLeaveCanvas = function() {
            setStat('cursor', null);
            tooltipEl && (tooltipEl.style.display = 'none');
        };
        
        $scope.mouseEnterCanvas = function() {
            tooltipEl && (tooltipEl.style.display = 'block');
        };
        
        
        //Set up a gradient to color-code the graph by scale:
        var scaleGradient = new Rainbow();
        scaleGradient.setNumberRange(0, Math.log(5478637500000)); //1 day - ~15 Billion years
        scaleGradient.setSpectrum('FFDCBF', '94A1BF');
        // return '#'+r.colorAt(2);
        
        var chartOpts = {
            type:'line',
            data:{
                datasets:[
                    {
                        pointRadius:0,
                        cubicInterpolationMode:'monotone',
                        // backgroundColor:'rgba(162, 151, 194, 0.5)', //A297C2 purple
                        // backgroundColor:'rgba(255, 220, 191, 0.7)', //FFDCBF peach
                        backgroundColor:'rgba(148, 161, 191, 0.4)',//94A1BF blue
                        data:[]
                    }
                ]
                
            },
            options:{
                animation:{duration:0},
                hover:{animationDuration:0},
                responsiveAnimationDuration:0,
                
                maintainAspectRatio:false,
                responsive:false,
                // onHover:onHover,
                legend:{display:false},
                tooltips:{
                    enabled:false,
                    mode:'index',
                    intersect:false,
                    custom:customTooltip,
                    // callbacks:{label:getTooltipLabel}
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        id:'primary-x-axis',
                        ticks:{
                            reverse:true,
                            callback: getTickLabel
                        }
                    }]
                },
                onClick:onChartClick,
                annotation:{
                    // Should be one of: afterDraw, afterDatasetsDraw, beforeDatasetsDraw
            		// See http://www.chartjs.org/docs/#advanced-usage-creating-plugins
            		drawTime: 'afterDatasetsDraw', // (default)
            		events: ['click'],
            		annotations:[]
                }
            }
        };
        
        
        const updateFractalDimension = function() {
            return timewave.getFractalDimension().then(fd=>{
                console.log(fd);
                windowStats.fractalDimension = fd;
            })
        }
        
        //Apply data from timewave object to the chart
        const applyChartData = function() {
            var dataArr = timewave.dataPoints;
            
            myChart.data.datasets[0].data = dataArr;
            
            // console.log('windowRange', timewave.windowRange);
            var size = timewave.getWindowSize();
            myChart.data.datasets[0].backgroundColor = '#'+scaleGradient.colorAt(Math.log(size));
            
            var ticks = myChart.options.scales.xAxes[0].ticks;
            ticks.max = dataArr[0].x;
            ticks.min = dataArr[dataArr.length-1].x;
            
            setStat('left', dataArr[0]);
            setStat('right', dataArr[dataArr.length-1]);
            
            
            _.assign(windowStats, timewave.windowStats);
            
            var range = timewave.windowRange;
            
            $scope.chartedFrom = timewave.formatDtz(range.start); 
            $scope.chartedTo = timewave.formatDtz(range.end);
            
            $scope.chartSpan = DateUtil.formatTimespan(range.start - range.end);
            
            if($scope.computeFractalDimension) {
                updateFractalDimension();
            }
            
            return $scope.getMarkers().then(myChart.update.bind(myChart));
            
        };
        
        const sizeChartContainer = function() {
            const heightDivs = ['#top-bar','#nav-bar','#toolbar','#stats-container'];
            const widthDivs = ['.left-sidebar', '.right-sidebar'];
            var otherHeights = 0, otherWidths = 0;
            heightDivs.forEach(function(selector) {
                let e = $(selector);
                otherHeights += e.outerHeight();
            });
            widthDivs.forEach(function(selector) {
                let e = $(selector);
                
                e.each(function(index){
                    if(this.className.indexOf('ng-hide') < 0) {
                        let w = $(this).outerWidth();
                        otherWidths += w;
                    }
                });
                
            });
            
            const doc = $(document);
            const targetHeight = doc.height() - otherHeights;
            const targetWidth = doc.width() - otherWidths;
            
            $('#myChart').width(targetWidth).height(targetHeight);
        };
        
        //destroy current chart and its DOM element and re-render
        //  (used on browser resize to make canvas element retain proper flexbox sizing)
        const initChart = function() {
            // console.log('Doing full render');
            if(myChart) {
                myChart.destroy();
            }
            dispState.rerender = false;
            
            var deferred = $q.defer();
            //use timeout to make sure DOM is rendered according to current scope
            $timeout(function() {
                sizeChartContainer();
                var ctx = document.getElementById("myChart").getContext('2d');
                myChart = new Chart(ctx, chartOpts);
                deferred.resolve(myChart);
            },50);
            return deferred.promise;
        };
        
        
        
        //Apply range from control panel UI on left sidebar
        $scope.applyRange = function() {
            var startWindow, endWindow;
            
            if($scope.rangePicker == 'dist') {
                let f = $scope.displayFromDtz;
                let t = $scope.displayToDtz;
                startWindow = f.magnitude*DateUtil.unitScale[f.unit];
                endWindow = t.magnitude*DateUtil.unitScale[t.unit];    
            }
            else {
                startWindow = timewave.getDtzForDate($scope.displayFromDate);
                endWindow = timewave.getDtzForDate($scope.displayToDate);
            }
            
            return timewave.setWindow({start:startWindow, end:endWindow}).then(applyChartData);
        };
        
        const getValueFromData = function(x) {
            //TODO binary search
            var data = myChart.data.datasets[0].data;
            for(let i=0; i < data.length-1; i++) {
                var a = data[i].x;
                var b = data[i+1].x;
                if(x >= b && x < a) {
                    return data[i].y;
                }
            }
        };
        
        
        
        var curr;
        
        const hightlightMarker = function(m) {
            if(m) {
                m.origColor = m.borderColor;
                m.borderColor = 'red';
                m.borderWidth = 2;
            }
        };
        const unhighlightMarker = function(m) {
            if(m) {
                m.borderColor = m.origColor;
                m.borderWidth = 1;
            }
        };
        const annotationClicked = function(pos, e) {
            
            var annotationArr = chartOpts.options.annotation.annotations;
            var annotationSpec = annotationArr[pos];
            console.log('annotationClicked!', annotationSpec);
            
            if(curr >= 0) {
                unhighlightMarker(annotationArr[curr]);
            }
            
            curr = pos;
            
            hightlightMarker(annotationSpec);
            
            $scope.currMarker = annotationSpec.marker;
            myChart.update();
            
        };
        
        
        $scope.getMarkers = function() {
            // console.log('adding markers');
            return NoonWebService.call('public/getTimeMarkers', {
                startWindow:timewave.windowRange.start,
                endWindow:timewave.windowRange.end,
                zeroDate:moment($scope.zeroDateJs).format('YYYY-MM-DD')
                
            }).then((markerList) => {
                $scope.markerList = markerList;
                
                $scope.removeMarkers();
                var annotationArr = chartOpts.options.annotation.annotations;
                
                
                var index = 1;
                
                _.forEach(markerList, m=>{
                    m.index = index++;
                    
                    var markerColor;
                    
                    if(m.isGroup) {
                        var len = m.markers.length;
                        var firstDate = m.markers[0] && m.markers[0].days_start;
                        var lastDate = m.markers[len-1] && m.markers[len-1].days_start;
                        m.dateLabel = dtzToLabel(firstDate)+' - '+dtzToLabel(lastDate);
                        m.name = '('+len+' events)';
                        markerColor = 'grey';
                    }
                    else {
                        m.dateLabel = dtzToLabel(m.days_start);
                        markerColor = '#337ab7';
                    }
                    
                    
                    if(dispState.showEvents) {
                        var a = {
                            type:'line',
                            mode:'vertical',
                            scaleID:'primary-x-axis',
                            borderColor:markerColor,
                            borderWidth:1,
                            value:m.days_start,
                            marker:m,
                            label:{
                                content:''+m.index,
                                fontSize:12,
                                position:'top',
                                yAdjust:5,
                                enabled:true
                            }
                        };
                        // a.onClick = annotationClicked.bind(a, annotationArr.length);
                    
                        annotationArr.push(a);
                        m.annotationPos = annotationArr.length-1;
                    }
                });
                
            });
        }; //end $scope.addMarkers def
        
        $scope.removeMarkers = function() {
            var annotationArr = chartOpts.options.annotation.annotations;
            var restoreMarker = (userMarkerIdx > -1) ? annotationArr[userMarkerIdx] : false;
            
            annotationArr.splice(0, annotationArr.length);
            if(restoreMarker) {
                annotationArr.push(restoreMarker);
            }
        };
        
        
        //Render with the default parameters/options
        initChart().then($scope.applyRange);
        
        
        var reRenderChart = function() {
            dispState.rerender = true;
            $timeout(function(){
                initChart().then(applyChartData);
            }, 50);
        };
        
        $scope.toggleSidebar = function(leftright) {
            var key = leftright+'SidebarCollapsed';
            dispState[key] = !dispState[key];
            dispState.rerender = true;
            reRenderChart();
        };
        
        
        
        
        var resizer = new ResizeWatcher(reRenderChart);
        resizer.enable();
        
        $scope.toggleEventVisibility = function() {
            var enabled = dispState.showEvents = !dispState.showEvents;
            
            if(!enabled) {
                $scope.removeMarkers();
            }
            else {
                $scope.getMarkers();
            }
            
            applyChartData();
        };
        
        
        
        var paramStack = $scope.paramStack = [];
        
        
        
        var markerPos = false;
        
        const setUserMarker = $scope.setUserMarker = function(point) {
            var x = point && point.x;
            var annotationArr = chartOpts.options.annotation.annotations;
            
            if(markerPos) {
                annotationArr.splice(annotationArr.indexOf(userMarker), 1);
            }
            
            if(!annotationArr) {
                annotationArr = chartOpts.options.annotation.annotations = [];
            }
            
            markerPos = $scope.markerPos = userMarker.value = x;
            if(point) {
                userMarker.label.content = '> '+dtzToLabel(x)+' <';
                userMarkerIdx = annotationArr.push(userMarker) - 1;
            }
            setStat('marker', point);
            myChart.update();
        };
        
        const removeUserMarker = function() {
            if(userMarkerIdx > -1) {
                var annotationArr = chartOpts.options.annotation.annotations;
                annotationArr.splice(userMarkerIdx, 1);
                userMarkerIdx = -1;
                setStat('marker', null);
                myChart.update();
            }
        };
        
        
        
        $scope.chartClicked = function(point, shiftKey) {
            var x = point.x;
            
            if(markerPos && shiftKey) {
                //Region zoom-in
                
                var smaller = x < markerPos ? x : markerPos;
                var larger = x < markerPos ? markerPos : x;
                
                timewave.setWindow({start:larger, end:smaller})
                    .then(function() {
                        setUserMarker(null);
                        applyChartData();
                    });
            }
            else {
                setUserMarker(point);
            }
            
            
            
        };
        
        
        $scope.$watch('numberSet+zeroDateJs+waveFactor+invertPostZeroDate+computeFractalDimension', function(){
            timewave.setParameters($scope.zeroDateJs, $scope.numberSet, $scope.waveFactor, $scope.resolution, $scope.invertPostZeroDate).then(function() {
                setZeroDateDisp();
                myChart && applyChartData();
            })
        });
        
        
        $scope.scrollEvent = function(evt) {
            var e = evt.originalEvent;
            // console.log(e);
            
            var up = e.deltaY < 0;
            var down = e.deltaY > 0;
            
            if(up) {
                $scope.zoomIn(); //$scope.hoverPoint && $scope.hoverPoint.x
            }
            else if(down) {
                $scope.zoomOut();
            }
        };
        
        
        
        
        //////////////////////////////////////////////////
        // Toolbar
        //////////////////////////////////////////////////
        
        
        $scope.undo = function() {
            timewave.undo().then(applyChartData);
        };
        
        
        
        $scope.zoomIn = function(toPos) {
            return timewave.zoomIn($scope.zoomPercent, (toPos || markerPos)).then(applyChartData);
        };
        
        $scope.zoomOut = function() {
            return timewave.zoomOut($scope.zoomPercent).then(applyChartData);
        };
        
        $scope.shiftLeft = function() {
            return timewave.shiftLeft($scope.shiftPercent).then(applyChartData);
        };
        
        $scope.shiftRight = function() {
            return timewave.shiftRight($scope.shiftPercent).then(applyChartData);
        };
        
        $scope.upResonance = function() {
            return timewave.upResonance().then(applyChartData);
        };
        
        $scope.downResonance = function() {
            return timewave.downResonance().then(applyChartData);
        };
        
        var highlighted;
        $scope.selectMarker = function(m) {
            // console.log(m);
            
            const apos = m.annotationPos;
            var annotationArr = chartOpts.options.annotation.annotations;
            let a = annotationArr[apos];
            
            if(m === highlighted) {
                m.selected = false;
                highlighted = false;
                dispState.showEvents && unhighlightMarker(a);
            }
            else {
                m.selected = true;
                dispState.showEvents && hightlightMarker(a);
                if(highlighted) {
                    highlighted.selected = false;
                    dispState.showEvents && unhighlightMarker(annotationArr[highlighted.annotationPos]);
                }
                highlighted = m;
            }
            dispState.showEvents && myChart.update();
        };
        
        $scope.newEvent = function() {
            var d;
            if($scope.markerPos) {
                d = timewave.getDateForDtz($scope.markerPos);
            }
            KeypressWatcher.disable();
            EventEditorDialog.new(d).then($scope.getMarkers).then(function() {
                KeypressWatcher.enable();
            });
        };
        
        $scope.editEvent = function(m) {
            KeypressWatcher.disable();
            EventEditorDialog.edit(m, timewave).then($scope.getMarkers).then(function() {
                KeypressWatcher.enable();
            });
        };
        
        
        
        KeypressWatcher.onEsc(removeUserMarker);
        KeypressWatcher.onLeft($scope.shiftLeft);
        KeypressWatcher.onRight($scope.shiftRight);
        KeypressWatcher.onUp($scope.zoomIn.bind($scope, null));
        KeypressWatcher.onDown($scope.zoomOut);
        
        $document.on('keydown', function(event) {
            if(event.which === 16 && $scope.markerPos) {
                var chartElem = document.getElementById("myChart");
                chartElem.style.cursor = 'zoom-in';
            }
        });
        $document.on('keyup', function(event) {
            if(event.which === 16) {
                var chartElem = document.getElementById("myChart");
                chartElem.style.cursor = 'default';
            }
        });
        
    });
    
    
}